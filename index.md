---
layout: home

hero:
  name: "NixiN"
  text: "A webUI to configure NixOS machines"
  tagline: by the Distrilab
  image: ./logo-nixin.svg
  actions:
     - theme: brand
       text: Generate my NixOS configuration
       link: /configure
     - theme: alt
       text: Documentation 
       link: /about

features:
  - icon: 💫
    title: Easy configuration
  - icon: 📢
    title: Ready for federation
  - icon: ✌️
    title: Self-Hostable
  - icon: 📖 
    title: Open source
---

<script setup>
import { VPTeamMembers } from 'vitepress/theme'

const members = [
  {
    avatar: 'https://distrilab.fr/files/Equipe_12b_20211010003350_20211010003817.jpg',
    name: 'Fabrice Bellamy',
    title: 'System Administration',
    links: [
      { icon: 'github', link: 'https://github.com/Lab-8916100448256' },
      { icon: 'mastodon', link: 'https://mastodon.cc' },
      { icon: 'github', link: 'https://gitlab.com/lab.8916100448256' }
    ]
  },
  {
    avatar: 'https://avatars.githubusercontent.com/u/124845?v=4',
    name: 'Florian Schmitt',
    title: 'Web Development',
    links: [
      { icon: 'github', link: 'https://github.com/mrflos' },
      { icon: 'mastodon', link: 'https://mastodon.cc/mrflos' }
    ]
  }
]
</script>
<br />
<br />

## About 

NixiN is a webUI that allows non-technical users to configure NixOS machines and VMs easily. It offers pre-configured setups for various web applications, focusing on security and scalability using modern technologies. You can select use cases from bundles (like cloud storage or social networks), customize web services, and specify domains, resulting in a complete NixOS configuration stored in a git repository. The platform will support managing multiple configurations, build them and deploy them. It also provides tools for distributed services.

[Read more about NixiN](./about)

## Our Team

We are french engineers contributing to open sources projects, experimenting modern web infrastructures with [the Distrilab](https://distrilab.fr) and members of [CHATONS](https://chatons.org) french alternative web hosters collective.

<VPTeamMembers size="small" :members="members" />
