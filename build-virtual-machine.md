# Build images based on your generated configurations

Your Nix configurations are located in the `inventory` folder you can build and run the configuration file you want by running nixos-generate from the NixiN root folder. If the build succeeds, the last output is a command located in your nix store to run your vm.

## Images formats

Check <https://github.com/nix-community/nixos-generators?tab=readme-ov-file#supported-formats> to find available formats.

For example, for qemu-kvm runner

```bash
nixos-generate -c inventory/demo-configuration.nix -f vm
```

(nixos-generate is installed by nix-shell, check `shell.nix` to see the packages added)

## Cross compile

Check <https://github.com/nix-community/nixos-generators?tab=readme-ov-file#cross-compiling>.
