{ pkgs, ... }:

{
  networking.hosts = {
    "127.0.0.1" = [ "hedgedoc.nixin.local" ];
  };

  networking.hostName = "demo";
  networking.firewall = {
    allowedTCPPorts = [
      80
      443
    ];
  };

  services.hedgedoc = {
    enable = true;
    settings.domain = "hedgedoc.nixin.local";
    settings.port = 8001;
    settings.host = "0.0.0.0";
    settings.protocolUseSSL = false;
    settings.allowOrigin = [
      "localhost"
      "hedgedoc.nixin.local"
    ];
  };

  users.users.operator = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    initialPassword = "test";
  };

  security.acme.defaults.email = "contact@nixin.local";
  security.acme.acceptTerms = true;

  services.nginx = {
    enable = true;

    # Use recommended settings
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    # Only allow PFS-enabled ciphers with AES256
    sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";

    virtualHosts."hedgedoc.nixin.local" = {
      forceSSL = true;
      enableACME = true;
      root = "/var/www/hedgedoc";
      locations."/".proxyPass = "http://127.0.0.1:8001";
      locations."/socket.io/" = {
        proxyPass = "http://127.0.0.1:8001";
        proxyWebsockets = true;
        extraConfig = "proxy_ssl_server_name on;";
      };
    };
  };

  environment.systemPackages = with pkgs; [
    git
    wget
    tmux
    mosh
    htop
  ];

  system.stateVersion = "24.05";
}
