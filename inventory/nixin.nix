{
  pkgs,
  config,
  lib,
  ...
}:
let
  nixin-web = pkgs.stdenv.mkDerivation {
    pname = "nixin-web";
    version = "0.1-alpha";
    src = pkgs.fetchzip {
      url = "https://git.distrilab.fr/NixiN/nixin-web/actions/runs/85/artifacts/nixin-website.zip";
      hash = "sha256-+cgWvbmjV9xckRCeRaj1dWqowBRbe/5497FcoZW+5ec=";
      stripRoot = false;
    };
    dontConfigure = true;
    dontBuild = true;
    installPhase = ''
      mkdir -p $out
      cp -a -T $src $out
    '';
  };
in
{
  networking.hosts = {
    "127.0.0.1" = [ "nixin.chmok.net" ];
  };

  networking.hostName = "nixin";
  networking.domain = "chmok.net";
  networking.firewall = {
    allowedUDPPorts = [
      53 # forgejo-runner
      8098 # wireguard
    ];
    allowedTCPPorts = [
      22 # ssh
      80 # http
      443 # https
    ];
  };
  networking.nameservers = [
    "80.67.169.12"
    "2001:910:800::12"
    "80.67.169.40"
    "2001:910:800::40"
  ];
  networking.wg-quick.interfaces = {
    wg0 = {
      address = [ "10.42.0.9/32" ];
      privateKey = "2M0w52jHmX5AgPw4V7Kq1hoZaEWa7H6NBoPfy/RbanQ=";
      peers = [
        {
          publicKey = "2MZzEGJzA3HrwkHf91TaKJEHwCNyVvsTLWoIYHrCxhY=";
          presharedKey = "DjbQfcvrc1cfk0nQNGqak4QZr46MW9WEovNK170mg+A=";
          allowedIPs = [ "10.42.0.0/24" ];
          endpoint = "195.201.63.240:8098";
          persistentKeepalive = 15;
        }
      ];
    };
  };

  time.timeZone = "UTC";
  i18n.defaultLocale = "en_US.UTF-8";

  users.users.operator = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    initialPassword = "CHANGE ME !!!";
  };

  security.sudo.extraRules = [
    {
      users = [ "operator" ];
      commands = [
        {
          command = "ALL";
          options = [ "NOPASSWD" ]; # "SETENV" # Adding the following could be a good idea
        }
      ];
    }
  ];

  services.forgejo = {
    enable = true;
    database.type = "postgres";
    # Enable support for Git Large File Storage
    lfs.enable = true;
    settings = {
      server = {
        DOMAIN = "forge.chmok.net";
        # You need to specify this to remove the port from URLs in the web UI.
        ROOT_URL = "https://forge.chmok.net/";
        HTTP_PORT = 3000;
      };
      # You can temporarily allow registration to create an admin user.
      service.DISABLE_REGISTRATION = true;
      # Add support for actions, based on act: https://github.com/nektos/act
      actions = {
        ENABLED = true;
        DEFAULT_ACTIONS_URL = "github";
      };
      # Sending emails is completely optional
      # You can send a test email from the web UI at:
      # Profile Picture > Site Administration > Configuration > Mailer Configuration
      mailer = {
        ENABLED = true;
        SMTP_ADDR = "mail.chmok.net";
        FROM = "noreply@${config.services.forgejo.settings.server.DOMAIN}";
        USER = "noreply@${config.services.forgejo.settings.server.DOMAIN}";
        PASSWD = "CHANGE ME !!!";
      };
    };
  };

  virtualisation.containers.enable = true;
  virtualisation.podman = {
    enable = true;
    # Create a `docker` alias for podman, to use it as a drop-in replacement
    dockerCompat = true;
    # Required for containers under podman-compose to be able to talk to each other.
    defaultNetwork.settings.dns_enabled = true;
  };

  services.gitea-actions-runner = {
    package = pkgs.forgejo-runner;
    instances.default = {
      enable = true;
      name = "nixinrunner";
      url = "https://forge.chmok.net";
      token = "S3uBKr4HsnxILAVA40ikLCNdAdKYxqcIGoqH1ihA";
      labels = [
        # provide a debian base with nodejs for actions
        "debian-latest:docker://node:20-bookworm"
        # fake the ubuntu name, because node provides no ubuntu builds
        "ubuntu-latest:docker://node:20-bookworm"
        # nixos
        "nixos:docker://nixos/nix:latest"
        # provide native execution on the host
        #"native:host"
      ];
    };
  };

  services.openssh.enable = true;
  services.openssh.ports = [ 22 ];
  services.openssh.settings = {
    PermitRootLogin = "no";
    PasswordAuthentication = false;
    KbdInteractiveAuthentication = false;
  };
  security.acme.defaults.email = "contact@nixin.local";
  security.acme.acceptTerms = true;

  services.nginx = {
    enable = true;

    # Use recommended settings
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    # Only allow PFS-enabled ciphers with AES256
    sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";

    virtualHosts."forge.chmok.net" = {
      extraConfig = ''
        client_max_body_size 512M;
      '';
      forceSSL = true;
      enableACME = true;
      locations."/".proxyPass =
        "http://localhost:${toString config.services.forgejo.settings.server.HTTP_PORT}";
    };
  };

  systemd.services.forgejo.preStart =
    let
      forgejoCmd = "${lib.getExe config.services.forgejo.package}";
      adminCmd = "${forgejoCmd} admin user";
      pwd = "CHANGE ME !!!";
      user = "operator";
      mail = "root@forge.chmok.net";
    in
    ''
        # create admin
        ${adminCmd} create --admin --email "${mail}" --username ${user} --password "${pwd}" || true
        ## uncomment this line to change an admin user which was already created
        # ${adminCmd} change-password --username ${user} --password "${pwd}" || true

        # link forgejo runner and forgejo
        ${forgejoCmd} forgejo-cli actions register --keep-labels --name nixin-runner \
      --secret 7c31591e8b67225a116d4a4519ea8e507e08f71f || true

        # forgejo-runner create-runner-file --instance https://example.conf \
        #  --secret 7c31591e8b67225a116d4a4519ea8e507e08f71f
        # create nixin repository

        # create password-store repository
    '';

  environment.systemPackages =
    with pkgs;
    let
    in
    #nixin-web = buildNpmPackage {
    #  pname = "nixin-web";
    #  version = "0.1-alpha";
    #  src = fetchgit {
    #    url = "https://git.distrilab.fr/NixiN/nixin-web";
    #    rev = "30384e64854b04e1f51ba68c3874765e141e9fc1";
    #    hash = "sha256-ERx6GX/qaiZXMzaLXuWTq4FxJ3RYy/LtkC2/kstF9nw=";
    #  };
    #  makeCacheWritable = true;
    #  npmDepsHash = "sha256-0YIrAMbM1CtInq1XPs/5r8FhOzE7bKXMoN23vm/ihEA=";
    #  npmBuildScript = "docs:build";
    #};
    #indication = "the code above keep stuck because vite and nixos are'nt friends";
    [
      nixin-web
      git
      wget
      tmux
      mosh
      htop
      neovim
      pass
    ];

  services.nginx.virtualHosts."nixin.chmok.net" = {
    forceSSL = true;
    enableACME = true;
    locations."/".root = ''${nixin-web}'';
    locations."/".index = "index.html";
  };
  system.stateVersion = "24.05";
}
