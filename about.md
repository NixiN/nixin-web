# About NixiN

NixiN is a web UI allowing non-technical users to easily configure their machines and VMs with NixOS.

The idea is to provide turnkey NixOS configurations for federated and distributed web use cases.

The base configuration will provide a common layer of hosting infrastructure, carefully chosen to achieve our objectives of security, scalability, maintainability and robustness, using modern technologies such as btrfs or zfs, private networks (wireguard, yggdrasil) and distributed storage (like garage S3, eris, DAT or IPFS).

The users will first choose their use case(s) (for example collaborative writing, Fediverse social networks, personal data management, private cloud storage, email/web hosting, ...).

This choice produce a pre-selection of web services that users can accept as is or fully customize by enabling/disabling some of them or even provide custom NixOS configurations for experts.

The users will then indicate the domains they want to use, and their complete NixOS configuration will be generated, stored in their git repository, built and deployed.

The users will be able to manage any numbers of machines/configs and will have some helpers for setting up distributed services.

## Why NixiN ?

We would like to provide an easy way to maintain complex web services and that more people are retaking control about their digital life.
Self hosting from scratch is hard, but declarative server configuration made easy and a more federated approach to cloud could be the holy grail!

We love Debian and YunoHost projects, but those distributions are not designed for the latest federated technologies. 
NixOs is an amazing project, but the learning curve is hard, even for experts in computers.

**Nixin wants to be an in-between, providing an easy UI like YunoHost to manage our machines, but with the power of NixOS backed in.**

## What is the plan ?

**Easy plan in 3 steps** :

1. Make a reactive UI web app that generates the configuration.nix code
2. Make a backend app that can store and manipulate multiple configuration files per user.
The backend will use private git repositories to store the configurations and a password store.
3. Use CD/CI from the git forge, but test and build 

We are still in the first step for now.

Check [the roadmap](./roadmap) for more detailed information.

## Why this name ?

NixiN is a palindrome so it's funny, isn't it? And it can be interpreted as "Nix in Nix", which is a valid infrastructure schema for NixiN: a host running NixOS with NixiN could run NixOS VMs configurated by NixiN.

*Shout outs for [ppom](https://framagit.org/ppom) for inspiration for this name*.
