with import <nixpkgs> {};

stdenv.mkDerivation {
    name = "nixin";
    buildInputs = [
        nodejs # for vitepress
        nixos-generators # for building images
    ];
    shellHook = ''
        export PATH="$PWD/node_modules/.bin/:$PATH"
        npm install
        npm run
    '';
}
