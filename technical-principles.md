# Technical principles
This page lists the technical principles we adhere to for developing NixiN and the technological choices that we have made.
Most of the principles we list here are known best software development practices that are well documented on the web or in books. So we will not describe them in detail or argument on why we should follow them and they appear here only as title under which we are putting some technological choices
We regularly update this list of technological choices as the project develop

## KISS "Keep it simple, stupid!"
We will keep the system modular and keep the modules simples, following the [Unix philosophy](https://en.wikipedia.org/wiki/Unix_philosophy) and the [KISS principle](https://en.wikipedia.org/wiki/KISS_principle)

## Do not reinvent the wheel
Whenever possible we will use existing software instead of writing our own code. And will favor widely accepted standards instead of inventing our own file format, protocols, languages or frameworks.

- Obviously we will use NixOS as the base operating system
- We will use git for version control of source code and configurations
- For storing passwords and secrets we will use zx2c4 passwordstore coupled with a git forge
- For the forge we will initially use forgejo. At a latter time we may add support for alternative forges that have a more distributed nature, like [Radicle](https://radicle.xyz/), when they become mature enough.
- For deploying NixOS to remote machines we are considering using [krops](https://github.com/krebs/krops). But we are still evaluating it.
- For formatting disks we will favor modern filesystems with snapshotting capability like zfs and btrfs
- ...

## Eat your own dog food
The project is bootstrapped using an infrastructure that is based on Proxmox, Debian and YunoHost for hosting its website and git forge. Currently only our workstations and the forgejo action runner used for CI/CD are based on Nix. 
But the goal is to host the whole project using itself as soon as possible. That is using NixOS servers managed with the tools and principles developed within the NixiN project.

## Version control everything
Not only will we version control our source code, but we will integrate version control at the core of NixiN, so that all the user configurations generated when using NixiN will be stored in a version control repository.

## Continuous Integration / Continuous Delivery
In the same way as with version control, we will not only use CI/CD for developing NixiN. We will also integrate CI/CD as a core feature of NixiN for the users to test and deploy their machines configurations.

## Focus on user experience

## Prioritize security
- only open ports that are strictly necessary on the public interface. go through a VPN for everything else
- use fail2ban or reaction
- use a passwords manager
- ...

## No premature performance optimization
Use best practices to write efficient code but do not write overly complicated solutions based on a-priori thinking of performance issue.
Only optimize what has been tested to be an issue.

## Do fast prototypes and release cycles.
Even though we think that Rust would be a better language for developing the backend components of the project, we are starting the first version using Go because it is faster to develop with it and easier to find contributors with this languages.

## To flake or not to flake? 
There is a bit of controversy around flakes. They bring some interesting convenience when using NixOS and have spawned an extensive ecosystem. But they are not without drawbacks. We have decided to not use flakes for now. But we'll keep our architecture open for the users who want to use them.

## There is only one timezone
Experience has shown that using multiple time-zones for the servers of an infrastructure is a recipe for disaster. 
Also, even if using a single time zone, using the timezone of one country for an international project is a source of confusion and headaches for people. 
Especially when that timezone is subject to daylight saving changes that are causing the clock to jump 1 hour forward or backward twice a year.
The only sensible choice is to set the servers time to UTC and to translate the timestamps to the user's timezone when displaying them on an interface. 

This is strongly opinion based. We may not not all agree on the subject. This is why we will make sure that it is easy for the users to choose their preferred time zone for setting up their servers.
