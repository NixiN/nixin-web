#!/usr/bin/env bash
# push main to prod (which will trigger the deploy action)
CURR_BRANCH=$(git symbolic-ref --short HEAD)
if [[ ${CURR_BRANCH} == "main" ]]; then 
  echo pushing main to prod...
  git checkout prod
  git merge --ff-only main
  git push origin prod
  git checkout main
else
  echo "This script is meant to be run from the main branch!"
fi
