# Installation

## Requirements

For now, the NixiN installation process was just tested on a Linux distribution, on a computer with virtualisation capacities.
So only requirements are :

- A Linux system with virtualisation support
- With Nix installed <https://nix.dev/manual/nix/stable/installation/installation.html>

## Grab the code and build the dev environment

Get the latest version from the official repository

```bash
git clone https://git.distrilab.fr/NixiN/nixin-web
```

Go in the main folder and run the `shell.nix` to install dependencies  

```bash
cd nixin-web && nix-shell
```

## Run locally

From the development shell run `npm run docs:dev` to launch the website locally, in development mode.

The site should be accessible at <http://localhost:5173> (or any configured port).

