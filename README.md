# NixiN Web

## Building the web site
npm run docs:build

## Testing localy
npm run docs:dev

## Manually deploying to a server usign sftp
pushd .vitepress/dist/ ; echo "put -r *" | sftp -b - -N user@server.domain.tld:www ; popd

## Automatically deploying to a server with forgejo action
Development is taking place in the main branch, or in feature branches that are then merged into main. 
To deploy the web site to production server the changes in main branch are merged to the prod branch, which is triggering the deploy action, defined in .forgejo/workflows/deploy.yaml 
This action builds the web site (`npm run docs:build`) and then deploys it to the production server using sftp with public/private key authentiction

You can use the script in bin/push-to-prod.sh to perform the merge of main branch into prod branch 

if you forked this repository, you need to create the secrets needed by the deploy action (in repository settings->actions->secrets) :  
 - DEPLOY_USER : OS user to connect to production server 
 - DEPLOY_TARGET : sftp target where the web site has to be deployed, in the form of server.domain.tld:path
 - DEPLOY_SSH_PRIVATE_KEY : private ssh key used to authenticate with the server
and configure the server with the public key corresponding to DEPLOY_SSH_PRIVATE_KEY
