create a bridge configuration file, /tmp/virbro.xml :  
```xml
<network>
  <name>default</name>
  <uuid>9014424a-8422-40ec-94d1-9f593b98cb7a</uuid>
  <forward mode="nat"/>
  <bridge name="virbr0" stp="on" delay="0"/>
  <mac address="52:54:00:ed:60:11"/>
  <ip address="192.168.122.1" netmask="255.255.255.0">
    <dhcp>
      <range start="192.168.122.2" end="192.168.122.254"/>
    </dhcp>
  </ip>
</network>
```

apply bridge configuration :  
```sh
virsh net-define /tmp/virbr0.xml
virsh net-start virbr0
```

optionnaly, make it to auto start :  
```sh
virsh net-autostart virbr0
```


copy the VM start script 
```sh
cp /nix/store/iim2hsbfj7kbxfygn741ica52fka6xk3-nixos-vm/bin/run-hedgedoc-vm ./run-vm-2
```

change network options : 
```sh
<     -net nic,netdev=user.0,model=virtio -netdev user,id=user.0,"$QEMU_NET_OPTS" \
---
>     -net nic,netdev=user.0,model=virtio -netdev bridge,br=virbr0,id=user.0,"$QEMU_NET_OPTS" \
```



run the VM : 
```sh
QEMU_NET_OPTS=helper=/run/wrappers/bin/qemu-bridge-helper ./run-vm-2
```

