---
layout: doc
prev: false
next: false
sidebar: false
outline: false
aside: false
---
# Generate my configuration

<script setup>
import ConfigForm from './components/ConfigForm.vue'
</script>

<ConfigForm />
