import { defineConfig } from "vitepress";

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "NixiN",
  description: "A webUI to configure NixOS machines",
  base: "/",
  head: [
    [
      "link",
      { rel: "shortcut icon", href: "logo-nixin.svg", type: "image/svg" },
    ],
  ],
  lastUpdated: true,
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    footer: {
      message: "Handcrafted with ❤️ by the DistriLab",
      copyright: "Copyleft © 2024-present the DistriLab - AGPL3 licence",
    },
    logo: "/logo-nixin.svg",
    nav: [
      { text: "Home", link: "/" },
      { text: "Documentation", link: "/about" },
      { text: "Generate my configuration", link: "configure" },
    ],
    search: {
      provider: "local",
    },
    sidebar: [
      {
        text: "The NixiN Project",
        items: [
          { text: "About", link: "/about" },
          { text: "Technical principles", link: "/technical-principles" },
          { text: "Roadmap", link: "/roadmap" },
        ],
      },
      {
        text: "Getting started",
        items: [
          { text: "Local installation", link: "/installation" },
          { text: "Build virtual machine", link: "/build-virtual-machine" },
        ],
      },
    ],

    socialLinks: [
      { icon: "github", link: "https://git.distrilab.fr/NixiN" },
      //{ icon: 'mastodon', link: 'https://mastodon.cc/@mrflos' }
    ],
  },

  ignoreDeadLinks: [
    // ignore all localhost links
    /^https?:\/\/localhost/,
  ],
});
