# NixiN's roadmap

This project is still very young but the main steps are already identified.
The roadmap may move when priorities are evolving.

## v0.1

**This version targets builds for simple mono-server configuration that can contain multiple webapps**.

Use of [nixos-generators](https://github.com/nix-community/nixos-generators) to build images of any type of container or vm, for any architecture and installation ISO images.

The code is based on Vitepress, a static webapp that is used for documentation, and includes a dynamic component in vuejs for generating NixOS configuration files.

Nothing is saved server side.  The users must handler their configuration files on their own.

### Features

- networking options with wireguard configuration options
- nixpkgs selection
- download or copy/paste generated configuration.nix
- templates for custom bundles
- templates for custom configuration.nix
- ISO image of NixiN webUI, to install on barebone servers or VMs.   

### Proposed bundles

- Write collectively : Hedgedoc pads, FreshRss, Nextcloud
- Social Medias : GoToSocial, Peertube, Pixelfed, Lemmy
- Forge : Forgejo, Forgejo runners, NixiN 

## v0.2

**This version includes a go server backend that authenticate users, save their configuration in a git repository and their machines passwords in a personal git password store**.

### Features

- user management
- configurations can be versioned and edited.
- automatic creation of password store and machine inventory in a git repository
- automatic generation of encrypted password, store in personal password store, when a configuration that needs a password
- manual deploy with krops
- ISO image of NixiN distribution : webUI, forgejo, Go backend, to install on barebone servers or VMs.   

### Proposed bundles

- Private cloud storage : Nextcloud, Syncthing, Vaultwarden 

## v0.3

**Continuous Integration and Delivery integration**

to test and deploy configurations to production servers or personal computers
to build OCI containers / VM images

### Features

- use CI to verify and build machines before real deployment
- manage your deployments from your NixiN interface
- ISO image of your configurations   

### Proposed bundles

- Containerize your machine's services : microvm, NixiN, your VM build for your configurations 

## v0.4

**Bundles for clustered / distributed services**

### Proposed bundles

- Email / S3 / webhosting : garage, aerogramme
- Resilience, anti-censorship : eris, ygdrasil

## Hopefully in the nearest future

- simple monitoring of machines in inventory
- ability to list services on a machine and start/stop/restart them
- backups operations (view, download, restore)
- desktop webUI (build your kde plasma, gnome, hyprland, etc.. desktop configurations)
- Unix users and user's configurations management
